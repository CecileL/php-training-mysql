<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Ajouter une randonnée</title>
	<link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
</head>

<body>
	<a href="/read.php">Liste des données</a>
	<h1>Ajouter</h1>
	<form action="/create.php" method="post">
		<div>
			<label for="name">Name</label>
			<input type="text" name="name" value="">
		</div>

		<div>
			<label for="difficulty">Difficulté</label>
			<select name="difficulty">
				<option value="très facile">Très facile</option>
				<option value="facile">Facile</option>
				<option value="moyen">Moyen</option>
				<option value="difficile">Difficile</option>
				<option value="très difficile">Très difficile</option>
			</select>
		</div>

		<div>
			<label for="distance">Distance</label>
			<input type="text" name="distance" value="">
		</div>
		<div>
			<label for="duration">Durée</label>
			<input type="duration" name="duration" value="00:00:00">
		</div>
		<div>
			<label for="height_difference">Dénivelé</label>
			<input type="text" name="height_difference" value="">
		</div>
		<button type="submit" name="button">Envoyer</button>
	</form>

	<?php
	$dsn = 'mysql:host=localhost;dbname=reunion_island;port=3306;charset=utf8';
	$pdo = new PDO($dsn, 'root', 'Y.@nnick<3');

	if (!empty($_POST["name"]) && !empty($_POST["difficulty"]) && !empty($_POST["distance"]) && !empty($_POST["duration"]) && !empty($_POST["height_difference"])) {

		$stmt = $pdo->prepare("INSERT INTO hiking (name, difficulty, distance, duration, height_difference) VALUES (:name, :difficulty, :distance, :duration, :height_difference)");

		$stmt->bindParam(':name', $_POST["name"]);
		$stmt->bindParam(':difficulty', $_POST["difficulty"]);
		$stmt->bindParam(':distance', $_POST["distance"]);
		$stmt->bindParam(':duration', $_POST["duration"]);
		$stmt->bindParam(':height_difference', $_POST["height_difference"]);


		$stmt->execute();
		echo "La randonnée a été ajoutée avec succès.";

	} else {
		echo "Tous les champs ne sont pas renseignés.";
	}

	/*(
	
	);*/

	?>
</body>

</html>