<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ajouter une randonnée</title>
	<link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
</head>
<body>
	<?php
		$dsn = 'mysql:host=localhost;dbname=reunion_island;port=3306;charset=utf8';
		$pdo = new PDO($dsn, 'root', 'Y.@nnick<3');

		$id = $_GET['id'];
		$query = $pdo->query("SELECT * FROM hiking WHERE id = $id");

		$resultat = $query->fetchAll();

	?>
	<a href="/php-pdo/read.php">Liste des données</a>
	<h1>Ajouter</h1>
	<?php 
		foreach ($resultat as $key => $value) {
	?>

	<form action="" method="post">
		<div>
			<label for="name">Name</label>
			<input type="text" name="name" value="<?=$resultat[$key]['name']?>">
		</div>

		<div>
			<label for="difficulty">Difficulté</label>
			<select name="difficulty">
				<option selected="selected" value="<?=$resultat[$key]['difficulty']?>"><?=$resultat[$key]['difficulty']?></option>
				<option value="très facile">Très facile</option>
				<option value="facile">Facile</option>
				<option value="moyen">Moyen</option>
				<option value="difficile">Difficile</option>
				<option value="très difficile">Très difficile</option>
			</select>
		</div>
		
		<div>
			<label for="distance">Distance</label>
			<input type="text" name="distance" value="<?=$resultat[$key]['distance']?>">
		</div>
		<div>
			<label for="duration">Durée</label>
			<input type="duration" name="duration" value="<?=$resultat[$key]['duration']?>">
		</div>
		<div>
			<label for="height_difference">Dénivelé</label>
			<input type="text" name="height_difference" value="<?=$resultat[$key]['height_difference']?>">
		</div>
		<button type="button" name="button">Envoyer</button>
	</form>
<?php
	}
?>

</body>
</html>
